package unimib.it.assignment1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment1pswApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment1pswApplication.class, args);
	}

}
