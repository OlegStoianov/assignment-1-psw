package unimib.it.assignment1.Controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import unimib.it.assignment1.Model.Accesso;
import unimib.it.assignment1.Service.AccessiService;

@RestController
public class HelloController {
	
	@Autowired
	private AccessiService service;
	
	@RequestMapping("/")
    public String index(){
		service.putAccesso(new Accesso(new Timestamp(System.currentTimeMillis())));
		return "La lista degli accessi" + service.getAccessi();
    }
	
	
	
	@RequestMapping("/accessi")
	public int numAccessi(){
		return service.getAccessi().size();
	}
	
	
	
	@RequestMapping("/unit")
	public String unitTest(){
		return "Unit test!";
	}
	
	

}
