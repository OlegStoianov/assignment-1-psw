package unimib.it.assignment1.Model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "accessi")
public class Accesso {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	
	private Timestamp orario;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Timestamp getOrario() {
		return orario;
	}


	public void setOrario(Timestamp orario) {
		this.orario = orario;
	}


	public Accesso(Long id, Timestamp orario) {
		super();
		this.id = id;
		this.orario = orario;
	}


	public Accesso() {
		super();
	}


	@Override
	public String toString() {
		return "Accessi [id=" + id + ", orario=" + orario + "]";
	}


	public Accesso(Timestamp orario) {
		super();
		this.orario = orario;
	}
	
	
		
}
