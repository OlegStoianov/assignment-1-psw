package unimib.it.assignment1.Service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import unimib.it.assignment1.Model.Accesso;
import unimib.it.assignment1.Repository.AccessiRepository;

@Service
public class AccessiService {

	@Autowired
    private AccessiRepository repository;

   
	
	
	public List<Accesso> getAccessi(){
		List<Accesso> accessi = new ArrayList<>();
		for (Accesso a : repository.findAll()){
			accessi.add(a);
		}
		return accessi;
	}




	public void putAccesso(Accesso accesso) {
		repository.save(accesso);
	}
}
