package unimib.it.assignment1.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import unimib.it.assignment1.Model.Accesso;

@Repository
public interface AccessiRepository extends CrudRepository<Accesso,Long>{

}
