package unimib.it.assignment1.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import unimib.it.assignment1.Controller.HelloController;

public class HelloControllerTest {
	
	 @Test
	    public void testHomeController() {
	        HelloController helloController = new HelloController();
	        String result = helloController.unitTest();
	        assertEquals(result, "Unit test!");
	    }

}
