package unimib.it.assignment1.test;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import unimib.it.assignment1.Model.Accesso;
import unimib.it.assignment1.Repository.AccessiRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {HelloControllerIT.Initializer.class})
public class HelloControllerIT {
	
	
	
	@Value("${spring.datasource.dbname}")
	private static String dbName;
	
	@Value("${spring.datasource.username}")
	private static String dbUsername;
	
	@Value("${spring.datasource.password}")
	private static String password;
	

	@ClassRule
	public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres")
	.withDatabaseName(dbName)
	.withUsername(dbUsername)
	.withPassword(password);
		
	@Autowired
	private AccessiRepository accessiRepository;
	
	@Test
    public void contextLoads() {
    }
	
	
	@Test
	public void controlAccessi(){
	    insertAccessi();
	    assertEquals(accessiRepository.count(), 2L);
	}
	
	
	public void insertAccessi(){
		accessiRepository.save(new Accesso(new Timestamp(System.currentTimeMillis())));
		accessiRepository.save(new Accesso(new Timestamp(System.currentTimeMillis())));
	}
	
	static class Initializer
	implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(
					"spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
					"spring.datasource.username=" + postgreSQLContainer.getUsername(),
					"spring.datasource.password=" + postgreSQLContainer.getPassword()
					).applyTo(configurableApplicationContext.getEnvironment());
		}
	}
}