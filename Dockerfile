FROM openjdk:11-jdk-slim

WORKDIR /usr/app
COPY ./target/assignment1-0.0.1-SNAPSHOT.jar ./assignment1.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "assignment1.jar"]

