# Assignment 1 - Processo e Sviluppo del Software

# Membri

- Stoianov Oleg (829519)
- Gusmara Andrea (831141)
- Villani Alessio (830075)

# Introduzione

Il primo Assignment del corso di Processo e Sviluppo del Software si pone come obiettivo la realizzazione di una Pipeline CI CD che automatizzi il processo di manutenzione di un'applicazione seguendo l'insieme di pratiche DEVOPS, mirando ad abbreviare il ciclo di vita di sviluppo di un sistema e soprattutto fornendo una consegna continua di software qualitativamente elevato.

# Applicazione

Siccome lo scopo dell'assignment non era lo sviluppo di un'applicazione in particolare, ma appunto la sua interazione con le Pipeline fornite da GitLab, abbiamo deciso di utilizzare una semplice applicazione che memorizza (secondo il formato "YYYY-MM-DD hh:mm:ss") gli accessi a quest'ultima.
Questa applicazione è stata realizzata attraverso Apache Maven sfruttando il framework Spring Boot di Java, mentre per quanto riguarda la persistenza dei dati ci siamo affidati a postgreSQL.


# Stages
Di seguito vengono elencate le fasi da implementare necessarie allo svolgimento dell'assignment:

- **Build**
- **Verify**
- **Unit-test**
- **Integration-test**
- **Package**
- **Release**
- **Deploy**

# Definizioni Globali

Se non viene specificata un'immagine all'interno di ogni stage, viene usata di default quella di maven:3.6.3

```maven
image: maven:3.6.3-jdk-11
```
Variabili di ambiente globali disponibili all'interno di ogni job:

```maven
variables:
MAVEN_CLI_OPTS: "--strict-checksums --threads 1C --batch-mode"
MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
DOCKER_HOST: "tcp://docker:2375"
DOCKER_DRIVER: overlay2
IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
```

# Build

La build del progetto viene realizzata sfruttando l'apposito goal **compile** di Maven:

```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS compile
```

# Verify

In questo secondo stage ci affidiamo a due plugins ([Checkstyle](https://checkstyle.sourceforge.io/) e [Spotbugs](https://spotbugs.github.io/)) per l'analisi di stile del codice e per l'ispezione di eventuali bug o code smells, sfruttando gli appositi goals in Maven.

Uso di Checkstyle con goal check:
```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS checkstyle:check
```
Uso di Spotbugs con goal check:
```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS spotbugs:check
```

Sfruttiamo in questo passaggio una cache per permettere la comunicazione tra jobs diversi, per natura indipendenti tra loro, garantendo il **save-restore** di quest'ultima in step successivi, questo non solo migliora l'efficenza del singolo job ma permette di non eseguire nuovamente istruzioni che sarebbero ridondanti in questo stage.

```maven
 cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
      - target/

```
# Test

Per quanto riguarda la fase di test abbiamo deciso di separare le tipologie da implementare in questo assignment, come richiesto nelle specifiche del progetto.
Ci siamo serviti di due plugin per automatizzare il processo di testing:
- [Surefire](https://maven.apache.org/surefire/maven-surefire-plugin/): per i test di unità.
- [Failsafe](https://maven.apache.org/surefire/maven-failsafe-plugin/): per i test di integrazione.

Di seguito proponiamo la spiegazione per queste categorie di test.

## Unit-test

Un test di unità si pone come obiettivo quello di verificare il corretto funzionamento di un solo modulo all'interno di un sistema, ad esempio un singolo metodo o funzione, nel nostro caso, data la libertà nella scelta del metodo da testare, abbiamo creato una semplice classe HelloControllerTest.java che si interfacciasse con l'apposito Controller (HelloController.java) per recuperare una stringa (attraverso il metodo: String unitTest()) ed effettuare l'assert su quest'ultima.

Visto che il goal **verify** di Maven esegue implicitamente anche la fase di test (sia unit che integration) e di package abbiamo deciso di sfruttare quest'ultimo per adempire al nostro scopo, posponendo il parametro "-DskipITs=true" al comando, per **non** eseguire anche i test di integrazione.

```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS verify -DskipITs=true
```
## Integration-test

In un test di integrazione abbiamo come scopo quello di testare il coordinamento tra due o più (macro)componenti, nel nostro caso, tra database postgresql e applicazione.
All'interno della classe HelloControllerIT.java viene prima stabilita una connessione col database, successivamente vengono inseriti nel database due accessi e si controlla tramite assert che questi siano effettivamente il numero che ci aspettavamo.

Come anticipato per i test di unità abbiamo lo stesso goal **verify** ma stavolta seguito dal parametro "-DskipUTs=true" per **non** eseguire i test di unità in questo stage.
```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS verify -DskipUTs=true
```
# Package

Nello stage di package tutte i file/librerie/dipendenze utilizzate dall'applicazione vengono compresse in un unico file con estensione .jar per facilitarne la distribuzione, in quanto un unico file "compresso" richiede un minore sforzo per essere trasferito sulla rete ed elaborato da un calcolatore.
La compressione non altera la struttura dei file stessi e inoltre slega l'applicazione dalla piattaforma nativa a patto di sfruttare una JVM per la sua esecuzione.
In maven questo viene realizzato attraverso l'apposito goal **package**:

```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS -DskipTests clean package spring-boot:repackage
```
Viene quindi prodotto un unico file .jar all'interno della cartella target pronto per essere rilasciato e distribuito.

```maven
paths:
    - target/*.jar
```

# Release

Durante lo stage di release ci affidiamo a Docker, in particolare a [Docker Engine](https://docs.docker.com/engine/) per la creazione automatica di un'immagine a partire dalle istruzioni contenute nel Dockerfile e, ovviamente, dal file .jar della nostra applicazione ottenuto nello stage di package.
Grazie ai servizi di virtualizzazione a livello di sistema operativo offerti da Docker (Platform as a Service) siamo in grado di avere un container per la nostra applicazione che potrà essere runnato su qualsiasi server.

Connessione al GitLab Container Registry:

```maven
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME
```

# Deploy

Infine, per quest'ultima fase sfruttiamo il servizio di cloud platforming (PaaS) [Heroku](https://www.heroku.com/), il quale permette di distribuire un [punto di accesso](https://assignment-1-psw.herokuapp.com/) alla nostra applicazione in un ambito cloud completamente slegato dalla piattaforma client che effettua l'accesso.

```console
$ apt-get update -qy
$ apt-get install -y ruby-dev
$ gem install dpl
$ dpl --provider=heroku --app=assignment-1-psw --api-key=$HEROKU_API_KEY
```

# Problemi Riscontrati

Durante lo stage di **verify** avevamo all'inizio pensato di sfruttare [SonarCloud](https://www.sonarcloud.io/) per l'analisi di stile del codice, con l'apposito comando **mvn $MAVEN_CLI_OPTS $MAVEN_OPTS verify sonar:sonar** in Maven, ma il comando verify si presenta più adatto alla fase di test, come indicato nella [documentazione](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html), quindi abbiamo deciso di utilizzare la soluzione proposta in MyDummyPipeline (Checkstyle e Spotbugs).
